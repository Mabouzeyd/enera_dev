import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { DiagnosticsDataDevice } from '../diagnostics-data-devices/diagnostics-data-devices.component';
import { DiagnosticsDataDeviceReading } from '../diagnostics-data-devices-readings/diagnostics-data-devices-readings.component';

@Component({
  selector: 'app-diagnostics-data-devices-reading-details',
  templateUrl: './diagnostics-data-devices-reading-details.component.html'
})

export class DiagnosticsDataDevicesReadingDetailsComponent {
  public diagnosticsDataDevicesReadingDetails: DiagnosticsDataDevicesReadingDetails;

    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: ActivatedRoute) {
        this.router.params.subscribe(x => {
          http.get<DiagnosticsDataDevicesReadingDetails>(baseUrl + 'api/reading/' + x.id + '/diagnosticsdata').subscribe(result => {
                console.log(result);
            console.log(x.id);
            this.diagnosticsDataDevicesReadingDetails = result;
            }, error => console.error(error));
        });
    }
}

export interface DiagnosticsDataDevicesReadingDetails {
  list: DiagnosticsDataDevice[];
}

