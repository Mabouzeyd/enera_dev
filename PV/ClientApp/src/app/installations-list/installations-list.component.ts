import { Component, Inject, Input } from '@angular/core';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { InstallationDetailsComponent } from '../installation-details/installation-details.component';

@Component({
    selector: 'app-installations-list',
    templateUrl: './installations-list.component.html'
})

export class InstallationsListComponent {
    public installations: Installation[];

    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
        http.get<Installation[]>(baseUrl + 'api/supplierintegration').subscribe(result => {
            console.log(result);
            this.installations = result;
        }, error => console.error(error));
    }
}

export interface Installation {

    idSite: number;
    accessLevel: number;
    owner: boolean;
    isAdmin: boolean;
    name: string;
    identifier: string;
    idUser: number;
    pvMax: string;
    timezone: string;
    phonenumber: string;
    geofenceEnabled: boolean;
    hasMains: boolean;
    hasGenerator: string;
    alarmMonitoring: number;
    invalidVRMAuthTokenUsedInLogRequest: string;
    invalidMqttPasswordSentAt: string;
    deviceIcon: string;
    syscreated: any;

}
