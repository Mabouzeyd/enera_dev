﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PV.Common.Exceptions;
using PV.WEB.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PV.WEB.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong: {ex}");
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            var error = new ApiErrorResponse()
            {
                Message = "Something went wrong. Please try again later.",
                ErrorCode = "server_error"
            };

            if (exception is BaseException)
            {
                var custom = exception as BaseException;
                error.ErrorCode = custom.Code;
                error.Message = custom.Message;
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            return context.Response.WriteAsync(error.ToString());
        }
    }
}