﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PV.DataAccess.Migrations
{
    public partial class KwhTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "KwhReading",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    PbValue = table.Column<decimal>(nullable: false),
                    PcValue = table.Column<decimal>(nullable: false),
                    GbValue = table.Column<decimal>(nullable: false),
                    GcValue = table.Column<decimal>(nullable: false),
                    PgValue = table.Column<decimal>(nullable: false),
                    BcValue = table.Column<decimal>(nullable: false),
                    KwhValue = table.Column<decimal>(nullable: false),
                    ReadingDate = table.Column<DateTimeOffset>(nullable: false),
                    SiteId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KwhReading", x => x.Id);
                    table.ForeignKey(
                        name: "FK_KwhReading_Site_SiteId",
                        column: x => x.SiteId,
                        principalTable: "Site",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AppSettings",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsDeleted", "Key", "UpdatedBy", "UpdatedOn", "Value" },
                values: new object[,]
                {
                    { new Guid("80eef905-58dd-4a77-acf6-7ff162de8080"), null, new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), false, "SelaataNextKwh", null, null, "1/1/2020 12:00:00 AM +00:00" },
                    { new Guid("41a1ad0b-fb47-41fe-95e1-4e85524b9c23"), null, new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), false, "FermesNextKwh", null, null, "1/1/2020 12:00:00 AM +00:00" },
                    { new Guid("ec3cb7b1-3563-4670-88f5-2df358a4c694"), null, new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), false, "RabihMNextKwh", null, null, "1/1/2020 12:00:00 AM +00:00" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_KwhReading_SiteId",
                table: "KwhReading",
                column: "SiteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "KwhReading");

            migrationBuilder.DeleteData(
                table: "AppSettings",
                keyColumn: "Id",
                keyValue: new Guid("41a1ad0b-fb47-41fe-95e1-4e85524b9c23"));

            migrationBuilder.DeleteData(
                table: "AppSettings",
                keyColumn: "Id",
                keyValue: new Guid("80eef905-58dd-4a77-acf6-7ff162de8080"));

            migrationBuilder.DeleteData(
                table: "AppSettings",
                keyColumn: "Id",
                keyValue: new Guid("ec3cb7b1-3563-4670-88f5-2df358a4c694"));
        }
    }
}
