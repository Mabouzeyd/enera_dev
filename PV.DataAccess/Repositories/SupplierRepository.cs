﻿using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class SupplierRepository : Repository<Supplier> , ISupplierRepository
    {
        public SupplierRepository(DatabaseContext databaseContext) : base(databaseContext)
        {

        }
    }
}
