﻿using System;
using Newtonsoft.Json;

namespace PV.Core.Models
{
    public class M3LoadProfiles : BaseModel
    {
        public string FeederName { get; set; }
        public string PrimarySubStation { get; set; }
        public string MeterID { get; set; }
        public DateTime ReferenceTimeStamp { get; set; }
        public DateTime MeterTimeStamp { get; set; }
        public double TotalActiveEnergyImport { get; set; }
        public double TotalReactiveEnergyImport { get; set; }
        public double ApparentPower1 { get; set; }
        public double ApparentPower2 { get; set; }
        public double ApparentPower3 { get; set; }
        public double ActivePower1 { get; set; }
        public double ActivePower2 { get; set; }
        public double ActivePower3 { get; set; }
        public decimal PowerFactor1 { get; set; }
        public decimal PowerFactor2 { get; set; }
        public decimal PowerFactor3 { get; set; }
    }
}
