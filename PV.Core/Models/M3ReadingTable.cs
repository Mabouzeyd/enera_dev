﻿
using System;

namespace PV.Core.Models
{
    public class M3ReadingTable : BaseModel
    {
        public string FeederID { get; set; }
        public DateTimeOffset? ReadingDate  { get; set; }
        public DateTimeOffset? DateOfGreatestCurrent  { get; set; }
        public decimal? ActiveEnergy  { get; set; }
        public decimal? ActiveEnergyExport   { get; set; }
        public decimal? ReactiveEnergyImport  { get; set; }
        public decimal? MaxVoltage_Ph1   { get; set; }
        public decimal? MaxVoltage_Ph2  { get; set; }
        public decimal? MaxVoltage_Ph3   { get; set; }
        public decimal? MaxCurrent_Ph1  { get; set; }
        public decimal? MaxCurrent_Ph2   { get; set; }
        public decimal? MaxCurrent_Ph3  { get; set; }
        public decimal? GreatestCurrentReached { get; set; }
        public decimal? MinVoltage_Ph1   { get; set; }
        public decimal? MinVoltage_Ph2  { get; set; }
        public decimal? MinVoltage_Ph3   { get; set; }
        public string ModifyDate  { get; set; }
        public int? ReptRecordNum { get; set; }
    }
}