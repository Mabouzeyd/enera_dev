﻿namespace PV.Core.Models
{
    public class AppSettings : BaseModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
