﻿using System.Threading;
using System.Threading.Tasks;
using PV.DataAccess.Abstractions.IRepositories;

namespace PV.DataAccess.Abstractions.IUnitOfWork
{
    public interface IUnitOfWork
    {
        IAppSettingsRepository AppSettings { get; }
        ISupplierRepository Suppliers { get; }
        IDiagnosticsDataRepository DiagnosticsDatas { get; }
        IDiagnosticsDataEnumValueRepository DiagnosticsDataEnumValues { get; }
        IReadingRepository Readings { get; }
        ISiteRepository Sites { get; }
        ICsvReadingRepository CsvReadings { get; }
        IKwhReadingRepository KwhReadings { get; }
        Task SaveChangesAsync(CancellationToken ct);
    }
}
