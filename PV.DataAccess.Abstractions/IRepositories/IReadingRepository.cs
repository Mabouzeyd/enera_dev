﻿using System;
using System.Threading;
using System.Threading.Tasks;
using PV.Core.Models;

namespace PV.DataAccess.Abstractions.IRepositories
{
    public interface IReadingRepository : IRepository<Reading>
    {
        Task<Reading> GetReadingWithDiagnosticsData(Guid readingId, CancellationToken ct);
    }
}