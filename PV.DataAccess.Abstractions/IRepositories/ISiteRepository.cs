﻿using System.Threading;
using System.Threading.Tasks;
using PV.Core.Models;

namespace PV.DataAccess.Abstractions.IRepositories
{
    public interface ISiteRepository : IRepository<Site>
    {
        Task<Site> GetSiteReadingsByExternalId(int siteId, CancellationToken ct);
    }
}