﻿using System.Collections.Generic;
using PV.Core.Models;

namespace PV.Business.DTO
{
    public class ReadingDetailsDTO
    {
        public Reading Reading { get; set; }
        public List<DiagnosticsData> DiagnosticsDataDetails { get; set; }
    }
}
