﻿using System;

namespace PV.Business.DTO
{
    public class PayrollIntegratorDTO
    {
        public string EmployeeId { get; set; }
        public DateTimeOffset InputDate { get; set; }
        public decimal NbCollectedBills { get; set; }
        public decimal NbDeliveredBills { get; set; }
        public decimal TotalCollectedBills { get; set; }
        public decimal TotalDeliveredBills { get; set; }
        public string PayElementCode { get; set; }
        public decimal Amount { get; set; }
        public decimal Quantity { get; set; }
        public string Notes { get; set; }
    }
}