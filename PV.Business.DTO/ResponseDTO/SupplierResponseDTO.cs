﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PV.Business.DTO
{
    public class SupplierResponseDTO
    {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
