﻿using System.Collections.Generic;

namespace PV.Business.DTO.IntegrationResponseDTO
{
    public class VictronDiagnosticDataRecord
    {
        public int IdSite { get; set; }
        public int? Timestamp { get; set; }
        public string Device { get; set; }
        public int Instance { get; set; }
        public int IdDataAttribute { get; set; }
        public string Description { get; set; }
        public string FormatWithUnit { get; set; }
        public object DbusServiceType { get; set; }
        public object DbusPath { get; set; }
        public string formattedValue { get; set; }
        public List<VictronDataAttributeEnumValue> DataAttributeEnumValues { get; set; }
        public int Id { get; set; }
    }
}