﻿using PV.Business.DTO.IntegrationResponseDTO.Base;

namespace PV.Business.DTO.IntegrationResponseDTO
{
    public class VictronInstallationsResponseIntegrationDTO : InstallationsIntegrationResponseDTO<VictronInstallationResponseIntegrationDTO>
    {
        public bool Success { get; set; }
    }
}
