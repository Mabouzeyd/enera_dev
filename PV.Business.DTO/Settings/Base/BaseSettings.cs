﻿namespace PV.Business.DTO.Settings.Base
{
    public class BaseSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string BaseUrl { get; set; }
    }
}
