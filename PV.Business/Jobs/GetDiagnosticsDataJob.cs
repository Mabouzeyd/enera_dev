﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines.Base;
using PV.Business.DTO.IntegrationResponseDTO;
using PV.Business.DTO.IntegrationResponseDTO.Victron;
using PV.Business.Settings.Victron;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IUnitOfWork;
using PV.Service.Business.Engines.Base;

namespace PV.Service.Business.Jobs
{
    public class GetDiagnosticsDataJob
    {
        public IUnitOfWork _uow { get; set; }
        public ILogger<BaseIntegrationEngine<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, DiagnosticsData, VictronReadingKwhDTO>> _logger { get; set; }
        public IBaseIntegrationEngine<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, DiagnosticsData, VictronReadingKwhDTO> _baseIntegrationEngine { get; set; }
        public GetDiagnosticsDataJob(IBaseIntegrationEngine<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, DiagnosticsData, VictronReadingKwhDTO> baseIntegration,
            ILogger<BaseIntegrationEngine<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, DiagnosticsData, VictronReadingKwhDTO>> logger,
            IUnitOfWork uow)
        {
            _logger = logger;
            _baseIntegrationEngine = baseIntegration;
            _uow = uow;
        }

        public async Task ExecuteAsync(CancellationToken ct)
        {
            var sites = await _baseIntegrationEngine.GetAllInstallationsAsync(ct);

            if (sites.Success)
            {
                var max = int.MaxValue;

                foreach (var site in sites.Records)
                {
                    var result = await _baseIntegrationEngine.GetDiagnosticsDataAsync(site.IdSite, max, ct);

                    if (result.Success && result.Records != null && result.Records.Any())
                        await _baseIntegrationEngine.SaveReadingData(site.IdSite, result, ct);
                }
            }
        }
    }
}
