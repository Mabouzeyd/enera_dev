﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PV.Business.DTO.IntegrationResponseDTO.Base;
using PV.Business.DTO.IntegrationResponseDTO.Victron;
using PV.Business.DTO.Settings.Base;
using PV.Common.Exceptions;
using PV.Common.Helpers;
using PV.Service.Business.Astractions.IServices;
using PV.Service.Business.Utils;
using RestSharp;

namespace PV.Service.Business.Services
{
    public class BaseIntegrationService<Settings, LoginResponseIntegrationDto, InstallationsIntegrationResponseDto, InstallationIntegrationResponseDto, ConnectedDeviceIntegrationReponseDto, DiagnosticsDataIntegrationDto, ReadingsKwhIntegrationDto>
        : IBaseIntegrationService<Settings, LoginResponseIntegrationDto, InstallationsIntegrationResponseDto, InstallationIntegrationResponseDto, ConnectedDeviceIntegrationReponseDto, DiagnosticsDataIntegrationDto, ReadingsKwhIntegrationDto>
        where Settings : BaseSettings
        where LoginResponseIntegrationDto : LoginResponseIntegrationDTO
        where InstallationsIntegrationResponseDto : InstallationsIntegrationResponseDTO<InstallationIntegrationResponseDto>
        where InstallationIntegrationResponseDto : InstallationResponseIntegrationDTO
        where ConnectedDeviceIntegrationReponseDto : ConnectedDevicesIntegrationDTO
        where DiagnosticsDataIntegrationDto : DiagnosticsDataIntegrationDTO
        where ReadingsKwhIntegrationDto : VictronReadingKwhDTO
    {
        private static string _token;
        private static string _idUser;
        private readonly BaseSettings _settings;
        public ILogger<BaseIntegrationService<Settings, LoginResponseIntegrationDto, InstallationsIntegrationResponseDto, InstallationIntegrationResponseDto, ConnectedDeviceIntegrationReponseDto, DiagnosticsDataIntegrationDto, ReadingsKwhIntegrationDto>> _logger { get; set; }
        public BaseIntegrationService(IOptions<BaseSettings> settings, ILogger<BaseIntegrationService<Settings, LoginResponseIntegrationDto, InstallationsIntegrationResponseDto, InstallationIntegrationResponseDto, ConnectedDeviceIntegrationReponseDto, DiagnosticsDataIntegrationDto, ReadingsKwhIntegrationDto>> logger)
        {
            _logger = logger;
            _settings = settings.Value;
        }

        public virtual async Task<LoginResponseIntegrationDto> AuthenticateSupplierAsync(CancellationToken ct)
        {
            _logger.LogInformation("Entered => BaseIntegrationService.Authenticate");

            var fullURL = $"{_settings.BaseUrl}{string.Format(BusinessConstants.VictronAuthenticationEndpoint)}";

            return await ExecuteRequestAsync<LoginResponseIntegrationDto>(fullURL, isLoggedIn: false, ct: ct);
        }

        public virtual async Task<InstallationsIntegrationResponseDto> GetAllInstallationsAsync(CancellationToken ct)
        {
            _logger.LogInformation("Entered => BaseIntegrationService.GetAllInstallations");

            if (string.IsNullOrWhiteSpace(_idUser))
                await AuthenticateSupplierAsync(ct);

            var fullURL = $"{_settings.BaseUrl}{string.Format(BusinessConstants.VictronAllInstallationsEndpoint, _idUser)}";

            return await ExecuteRequestAsync<InstallationsIntegrationResponseDto>(fullURL, ct: ct);
        }

        public virtual async Task<ConnectedDeviceIntegrationReponseDto> GetConnectedDevicesAsync(int siteId, CancellationToken ct)
        {
            _logger.LogInformation("Entered => BaseIntegrationService.GetAllConnectedDevices");

            var fullURL = $"{_settings.BaseUrl}{string.Format(BusinessConstants.VictronConnectedDevicesEndpoint, siteId)}";

            return await ExecuteRequestAsync<ConnectedDeviceIntegrationReponseDto>(fullURL, ct: ct);
        }

        public async Task<DiagnosticsDataIntegrationDto> GetDiagnosticsDataAsync(int siteId, int count, CancellationToken ct)
        {
            _logger.LogInformation("Entered => BaseIntegrationService.GetDiagnosticsDataAsync");

            var fullURL = $"{_settings.BaseUrl}{string.Format(BusinessConstants.VictronDiagnosticsDataEndpoint, siteId, count)}";

            return await ExecuteRequestAsync<DiagnosticsDataIntegrationDto>(fullURL, ct, Method.GET);
        }

        public async Task<string> GetCsvReadingsAsync(int siteId, double start, double end, CancellationToken ct)
        {
            _logger.LogInformation("Entered => BaseIntegrationService.GetDiagnosticsDataAsync");

            var fullURL = $"{_settings.BaseUrl}{string.Format(BusinessConstants.VictronCsvReadingsEndpoint, siteId, start, end)}";

            var csvReading = await ExecuteSaveFileRequestAsync(fullURL, ct, Method.GET);

            return csvReading;
        }

        public async Task<ReadingsKwhIntegrationDto> GetReadingsKwhAsync(int siteId, double start, double end, CancellationToken ct)
        {
            _logger.LogInformation("Entered => BaseIntegrationService.GetDiagnosticsDataAsync");

            var fullURL = $"{_settings.BaseUrl}{string.Format(BusinessConstants.VictronReadingsKwhEndpoint, siteId, start, end)}";

            var csvReading = await ExecuteRequestAsync<ReadingsKwhIntegrationDto>(fullURL, ct, Method.GET);

            return csvReading;
        }

        public async Task<T> ExecuteRequestAsync<T>(string fullUrl, CancellationToken ct, Method method = Method.POST, bool? isLoggedIn = null)
        {
            if ((string.IsNullOrWhiteSpace(_token) || string.IsNullOrWhiteSpace(_idUser)) && (!isLoggedIn.HasValue || (isLoggedIn.Value)))
            {
                await this.AuthenticateSupplierAsync(ct);
            }

            var client = new RestClient(fullUrl);

            var authorization = string.Format(BusinessConstants.BearerString, _token);

            IRestRequest request;

            bool saveCredentials = false;

            if (isLoggedIn.HasValue && !isLoggedIn.Value)
            {
                request = new RestRequest(method)
                          .AddParameter("undefined", "{\n\t\"username\":\"" + _settings.Username + "\",\n\t\"password\":\"" + _settings.Password + "\"\n}", ParameterType.RequestBody);

                saveCredentials = true;
            }
            else
            {
                request = new RestRequest(method).AddHeader("X-Authorization", authorization);
            }

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                var result = JsonSerializerHelper.Deserialize<T>(response.Content);

                if (saveCredentials)
                {
                    var casted = result as LoginResponseIntegrationDto;

                    _token = casted.Token;

                    _idUser = casted.Id;
                }

                return result;
            }
            else
            {
                _idUser = null;
                _token = null;
                throw new BaseException(ExceptionConstants.SUPPLIER_AUTHENTICATION_FAILED_CODE, ExceptionConstants.SUPPLIER_AUTHENTICATION_FAILED_MESSAGE);
            }
        }

        public async Task<string> ExecuteSaveFileRequestAsync(string fullUrl, CancellationToken ct, Method method = Method.POST, bool? isLoggedIn = null)
        {
            if ((string.IsNullOrWhiteSpace(_token) || string.IsNullOrWhiteSpace(_idUser)) && (!isLoggedIn.HasValue || (isLoggedIn.Value)))
            {
                await this.AuthenticateSupplierAsync(ct);
            }

            var client = new RestClient(fullUrl);

            var authorization = string.Format(BusinessConstants.BearerString, _token);

            IRestRequest request;

            bool saveCredentials = false;

            if (isLoggedIn.HasValue && !isLoggedIn.Value)
            {
                request = new RestRequest(method)
                          .AddParameter("undefined", "{\n\t\"username\":\"" + _settings.Username + "\",\n\t\"password\":\"" + _settings.Password + "\"\n}", ParameterType.RequestBody);

                saveCredentials = true;
            }
            else
            {
                request = new RestRequest(method).AddHeader("X-Authorization", authorization);
            }

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                string result = response.Content;

                if (saveCredentials)
                {
                    var casted = result as LoginResponseIntegrationDto;

                    _token = casted.Token;

                    _idUser = casted.Id;
                }

                return result;
            }
            else
            {
                _idUser = null;
                _token = null;
                throw new BaseException(ExceptionConstants.SUPPLIER_AUTHENTICATION_FAILED_CODE, ExceptionConstants.SUPPLIER_AUTHENTICATION_FAILED_MESSAGE);
            }
        }
    }
}