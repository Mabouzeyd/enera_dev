﻿using System;
using System.Threading;
using System.Threading.Tasks;
using PV.Business.DTO.IntegrationResponseDTO.Base;
using PV.Business.DTO.IntegrationResponseDTO.Victron;
using PV.Business.DTO.Settings.Base;

namespace PV.Business.Abstractions.IEngines.Base
{
    public interface IBaseIntegrationEngine<Settings, LoginResponse, InstallationsResponse, InstallationResponse, ConnectedDevicesResponse, DiagnosticsDataResponse, DiagnosticsDataEntity, ReadingKwh>
        where LoginResponse : LoginResponseIntegrationDTO
        where InstallationsResponse : InstallationsIntegrationResponseDTO<InstallationResponse>
        where Settings : BaseSettings
        where InstallationResponse : InstallationResponseIntegrationDTO
        where ConnectedDevicesResponse : ConnectedDevicesIntegrationDTO
        where DiagnosticsDataResponse : DiagnosticsDataIntegrationDTO
        where ReadingKwh : VictronReadingKwhDTO
    {
        Task<LoginResponse> AuthenticateSupplierAsync(Guid supplierId, CancellationToken ct);
        Task<InstallationsResponse> GetAllInstallationsAsync(CancellationToken ct);
        Task<InstallationResponse> GetInstallationAsync(int idSite, CancellationToken ct);
        Task<ConnectedDevicesResponse> GetSiteConnectedDevicesAsync(int idSite, CancellationToken ct);
        Task<DiagnosticsDataResponse> GetDiagnosticsDataAsync(int idSite, int count, CancellationToken ct);
        Task SaveReadingData(int siteId, DiagnosticsDataResponse result, CancellationToken ct);
    }
}
